//
// ──────────────────────────────────────────────────── I ──────────
//   :::::: I M P O R T : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────
//
//
// ─── VENDOR ─────────────────────────────────────────────────────────────────────
//
import React from "react";
import ReactDOM from "react-dom";
import { applyMiddleware, createStore } from "redux";
import { Provider } from "react-redux";
import promise from "redux-promise";
import multi from "redux-multi";
import thunk from "redux-thunk";

// ────────────────────────────────────────────────────────────────────────────────
//
// ─── CUSTOM ─────────────────────────────────────────────────────────────────────
//
import App from "./Main";
import reducers from "./Main/reducers";
// ────────────────────────────────────────────────────────────────────────────────
// ────────────────────────────────────────────────────────────────────────────────

//
// ────────────────────────────────────────────────── II ──────────
//   :::::: S T O R E : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────
//
const devTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();
const store = applyMiddleware(thunk, multi, promise)(createStore)(
  reducers,
  devTools
);
// ────────────────────────────────────────────────────────────────────────────────

//
// ──────────────────────────────────────────────────────────── III ──────────
//   :::::: A P P   R E N D E R : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────
//
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("app")
);
// ────────────────────────────────────────────────────────────────────────────────
