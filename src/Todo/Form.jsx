//
// ────────────────────────────────────────────────────── I ──────────
//   :::::: I M P O R T S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//
//
// ─── VENDOR ─────────────────────────────────────────────────────────────────────
//
import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// ────────────────────────────────────────────────────────────────────────────────
//
// ─── CUSTOM ─────────────────────────────────────────────────────────────────────
//
import IconButton from "../Template/IconButton";
import Grid from "../Template/Grid";
import { add, changeDescription, search, clear } from "./todoActions";
// ────────────────────────────────────────────────────────────────────────────────
//
// ────────────────────────────────────────────────────────────────── II ──────────
//   :::::: R E D U X   M A P P I N G : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────────────────
//
const mapStateToProps = state => ({ description: state.todo.description });
const mapDispatchToProps = dispatch =>
  bindActionCreators({ changeDescription, search, add, clear }, dispatch);
// ────────────────────────────────────────────────────────────────────────────────
//
// ────────────────────────────────────────────────────────────────────────────────
//
// ──────────────────────────────────────────────────────────────────── III ──────────
//   :::::: F O R M   C O M P O N E N T : :  :   :    :     :        :          :
// ──────────────────────────────────────────────────────────────────────────────
//
class Form extends Component {
  componentDidMount = () => {
    this.props.search();
  };
  render = () => (
    <form className="form" onKeyUp={this.props.add}>
      <Grid cols="12 9 10">
        <input
          type="text"
          id="description"
          className="form-control"
          placeholder="Adiciona uma tarefa"
          onChange={this.props.changeDescription}
          value={this.props.description}
        />
      </Grid>
      <Grid cols="12 9 2">
        <IconButton
          style="primary"
          icon="plus"
          onClick={this.props.add.bind(this, this.props.description)}
        />
        <IconButton style="info" icon="search" onClick={this.props.search} />
        <IconButton style="default" icon="close" onClick={this.props.clear} />
      </Grid>
    </form>
  );
}
// ────────────────────────────────────────────────────────────────────────────────
// ────────────────────────────────────────────────────── IV ──────────
//   :::::: E X P O R T S : :  :   :    :     :        :          :
// ────────────────────────────────────────────────────────────────
//
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);
// ────────────────────────────────────────────────────────────────────────────────
