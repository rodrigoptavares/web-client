import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { done, pending, remove } from "./todoActions";
import IconButton from "../Template/IconButton";

const List = props => {
  const list = props.list || [];
  return (
    <table className="table">
      <thead>
        <tr>
          <th>Descrição</th>
          <th className="actions">Ações</th>
        </tr>
      </thead>
      <tbody>
        {list.map(todo => (
          <tr key={todo._id}>
            <td className={todo.done ? "done" : ""}>{todo.description}</td>
            <td>
              <IconButton
                style={`sucess`}
                icon="check"
                onClick={() => props.done(todo)}
                hide={todo.done}
              />
              <IconButton
                style={`warning`}
                icon="undo"
                onClick={() => props.pending(todo)}
                hide={!todo.done}
              />
              <IconButton
                style={`danger`}
                icon="trash-o"
                hide={!todo.done}
                onClick={() => props.remove(todo)}
              />
            </td>
          </tr>
        ))}
      </tbody>
    </table>
  );
};

const mapStateToProps = state => ({ list: state.todo.list });
const mapDispatchToProps = dispatch =>
  bindActionCreators({ done, pending, remove }, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(List);
