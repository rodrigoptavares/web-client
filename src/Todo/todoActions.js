import axios from "axios";

const URL = "http://localhost:3003/api/todos";

export const changeDescription = event => ({
  type: "DESCRIPTION_CHANGED",
  payload: event.target.value
});

export const search = () => (dispatch, getState) => {
  const description = getState().todo.description;
  const search = description ? `&description__regex=/${description}/` : "";
  axios.get(`${URL}?sort=-createdAt${search}`).then(res =>
    dispatch({
      type: "TODO_SEARCHED",
      payload: res.data
    })
  );
};

export const add = description => dispatch => {
  axios
    .post(URL, { description })
    .then(() => dispatch({ type: "TODO_ADDED" }))
    .then(() => dispatch(clear()))
    .then(() => dispatch(search()));
};

export const done = todo => dispatch => {
  axios
    .put(`${URL}/${todo._id}`, { ...todo, done: true })
    .then(() => dispatch({ type: "TODO_DONE" }))
    .then(() => dispatch(search()));
};

export const pending = todo => dispatch => {
  axios
    .put(`${URL}/${todo._id}`, { ...todo, done: false })
    .then(() => dispatch({ type: "TODO_PENDING" }))
    .then(() => dispatch(search()));
};

export const remove = todo => dispatch => {
  axios
    .delete(`${URL}/${todo._id}`)
    .then(() => dispatch({ type: "TODO_REMOVED" }))
    .then(() => dispatch(search()));
};

export const clear = () => [
  {
    type: "TODO_CLEARED"
  },
  search()
];
